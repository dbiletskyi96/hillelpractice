package com.hillel.dbiletskyi.interfaces;

public interface BookPrintable extends Printable
{
    void printBook();

    @Override
    default void defaultPrint()
    {

    }

    interface TestInterface{
        void printTest();
    }
}
