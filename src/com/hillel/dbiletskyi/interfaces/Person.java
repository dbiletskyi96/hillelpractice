package com.hillel.dbiletskyi.interfaces;

public class Person
{
    private String name;
    private int age;
    private String lastName;

    public Person()
    {
    }

    public Person setName(String name)
    {
        this.name = name;
        return this;
    }

    public Person setAge(int age)
    {
        this.age = age;
        return this;
    }

    public Person setLastName(String lastName)
    {
        this.lastName = lastName;
        return this;
    }
}
