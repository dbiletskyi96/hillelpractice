package com.hillel.dbiletskyi.interfaces;

public class Book implements BookPrintable.TestInterface,Printable,VeryPrintable
{
    @Override
    public void print()
    {
        System.out.println("Printable");
    }

    @Override
    public void printTwoTimes()
    {
        System.out.println("1");
        System.out.println("2");
    }

    public String getName(){
        return "name";
    }



    @Override
    public void printTest()
    {

    }
}
