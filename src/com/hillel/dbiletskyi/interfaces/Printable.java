package com.hillel.dbiletskyi.interfaces;

public interface Printable
{
    String DEFAULT_STRING_CONST = "default";

    void print();

    void printTwoTimes();

    default void defaultPrint()
    {
        System.out.println("Default");
        print();
        printTwoTimes();
    }

    static void printStatic()
    {
        System.out.println("static");
    }
}
