package com.hillel.dbiletskyi.interfaces;

public class Strategy
{
    private Printable _printable;

    public Strategy(Printable printable)
    {
        _printable = printable;
    }

    public void call(){
        _printable.print();
    }
}
