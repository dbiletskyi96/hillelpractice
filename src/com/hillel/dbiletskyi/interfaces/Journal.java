package com.hillel.dbiletskyi.interfaces;

public class Journal implements Printable
{
    @Override
    public void print()
    {
        System.out.println("Journal");
    }

    @Override
    public void printTwoTimes()
    {
        System.out.println("Journal1");
        System.out.println("Journal2");
    }
}
