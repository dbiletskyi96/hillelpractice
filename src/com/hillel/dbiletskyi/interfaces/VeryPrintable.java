package com.hillel.dbiletskyi.interfaces;

public interface VeryPrintable
{
    void print();

    void printTwoTimes();
}
