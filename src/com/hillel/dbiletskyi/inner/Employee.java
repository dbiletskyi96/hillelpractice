package com.hillel.dbiletskyi.inner;

public class Employee extends Person
{
    private int salary;

    public Employee(int salary)
    {
        System.out.println("Employee");
        this.salary = salary;
    }


    @Override
    public void getInfo()
    {
        System.out.println("Employee info");
    }

    public void getInfo(int q)
    {
        System.out.println("Employee info" + q);
    }
}
