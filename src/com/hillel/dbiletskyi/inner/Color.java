package com.hillel.dbiletskyi.inner;

public enum Color
{
    RED("red", new Client()), GREEN("green", new Employee(12));

    private String color;
    Color(String color, Person person)
    {
        this.color = color;
    }

    public String getColor(){
        return color;
    }

}
