package com.hillel.dbiletskyi.inner;

public class Client extends Person
{
    private String email;
    public int age;


    public Client(int age)
    {
        age = 15;
    }

    public Client()
    {
        age = 34444;
    }

    public void getInfo()
    {
        System.out.println("Client info");
    }

    public int getAge()
    {
        return age;
    }
}
