package com.hillel.dbiletskyi.inner;

public abstract class Person{

    public static final String CONSTANT_PERSON = "AAAAA";


    public int age;
    private Address address;

    public Person()
    {
        this.age = 123;
    }

    public static class Address{
        private String address;

        public Address()
        {
            System.out.println("2");
            this.address = "sa";
        }

        public void getInfo(){
            System.out.println("Info person");

        }

    }

    public abstract void getInfo();

    public int getAge(){
        return age;
    }

}

