package com.hillel.dbiletskyi;

import com.hillel.dbiletskyi.interfaces.Book;
import com.hillel.dbiletskyi.interfaces.Book3000;
import com.hillel.dbiletskyi.interfaces.BookPrintable;
import com.hillel.dbiletskyi.interfaces.Journal;
import com.hillel.dbiletskyi.interfaces.Person;
import com.hillel.dbiletskyi.interfaces.Printable;
import com.hillel.dbiletskyi.interfaces.SingletonClass;

public class Main
{

    public static void main(String[] args)
    {
        BookPrintable.TestInterface book1 = new BookPrintable.TestInterface()
        {
            @Override
            public void printTest()
            {

            }
        };

        SingletonClass singletonClass = SingletonClass.getInstance();
        SingletonClass singletonClass2 = SingletonClass.getInstance();

        Person person = new Person();
        person.setAge(12);
        person.setLastName("assa");
        person.setName("asas");

        Person person1 = new Person().setAge(12)
                                     .setLastName("asas")
                                     .setName("121");
        String aaaa= "a";//bufferedReader
        Printable printable = null;
        if(aaaa.equals("a")){
            printable = new Book();
        }
        if(aaaa.equals("b")){
            printable = new Book3000();
        }

        printable.print();


        /*book1.print();
        book1.defaultPrint();
        book1.printBook();*/

        Printable printable1 = new Printable()
        {
            @Override
            public void print()
            {
                System.out.println("anonim");
            }

            @Override
            public void printTwoTimes()
            {

            }
        };

        printable.print();

        ((Book) book1).getName();
        String _const = Printable.DEFAULT_STRING_CONST;
        Object book2 = new Book();
        ((Book) book2).defaultPrint();
        Book book3 = (Book) book1;
        book3.getName();
        book3.printTwoTimes();

        Printable journal = new Journal();

        // Journal journal1 = (Journal) book1;

        print((Book) book1);
        // print((Book) journal);

    }

    public static void print(Book object)
    {
        object.print();
        object.printTwoTimes();
    }
}
